﻿using Jardineria.Modelos;
using System.Windows.Forms;

namespace Jardineria
{
    public partial class VentanaPreciosLimite : Form
    {
        public VentanaPreciosLimite(Producto max, Producto min)
        {
            InitializeComponent();

            LabelProductoMax.Text = "(" + max.GetCodigo() + ")" + " " + max.GetNombre() + " a P.V.P. de " + max.GetPVP() + "€";
            LabelProductoMin.Text = "(" + min.GetCodigo() + ")" + " " + min.GetNombre() + " a P.V.P. de " + min.GetPVP() + "€";
        }
    }
}
