﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Jardineria.Error.gui
{
    public partial class VentanaError : Form
    {
        //
        // Atributo
        //
        private readonly Point CentroPanelDetalles;

        //
        // Constructor
        //
        public VentanaError(string titulo = "Error", string descripcion = "")
        {
            InitializeComponent();
            CentroPanelDetalles = BotonDesplegar.Location;

            TituloError.Text = titulo;
            AutoSize = true;

            // Comprueba si se ha establecido el cuerpo de la descripción.
            // En caso negativo, elimina el panel de detalles
            if (descripcion.Trim().ToString() == "")
            {
                TablaDistribucion.Controls.Remove(PanelDetalles);
                TablaDistribucion.RowCount--;

                Height = Height / 2;

            }
            else
            {
                // Establecer detalle del error
                Label LabelDescripcionError = new Label
                {
                    Text = descripcion,
                    TextAlign = ContentAlignment.MiddleCenter,
                    MaximumSize = new Size(210, 0),
                    AutoSize = true,
                    Location = new Point(CentroPanelDetalles.X / 2, CentroPanelDetalles.Y),
                    Visible = false
                };

                PanelDetalles.Controls.Add(LabelDescripcionError);
            }

        }

        //
        // Eventos
        //
        private void BotonDesplegar_Click(object sender, EventArgs e)
        {
            if (BotonDesplegar.Location == CentroPanelDetalles)
            {
                BotonDesplegar.Location = new Point(5, 5);
                BotonDesplegar.Text = "Ocultar detalles";

                // Hacer visible el detalle del error
                PanelDetalles.Controls[1].Visible = true;
            }
            else
            {
                BotonDesplegar.Location = CentroPanelDetalles;
                BotonDesplegar.Text = "Ver detalles";

                // Ocultar el detalle del error
                PanelDetalles.Controls[1].Visible = false;
            }
        }
    }
}
