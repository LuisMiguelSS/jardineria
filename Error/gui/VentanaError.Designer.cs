﻿namespace Jardineria.Error.gui
{
    partial class VentanaError
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VentanaError));
            this.TablaDistribucion = new System.Windows.Forms.TableLayoutPanel();
            this.IconoError = new System.Windows.Forms.PictureBox();
            this.TituloError = new System.Windows.Forms.Label();
            this.PanelDetalles = new System.Windows.Forms.Panel();
            this.BotonDesplegar = new System.Windows.Forms.Button();
            this.TablaDistribucion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IconoError)).BeginInit();
            this.PanelDetalles.SuspendLayout();
            this.SuspendLayout();
            // 
            // TablaDistribucion
            // 
            this.TablaDistribucion.ColumnCount = 2;
            this.TablaDistribucion.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.13235F));
            this.TablaDistribucion.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 62.86765F));
            this.TablaDistribucion.Controls.Add(this.IconoError, 0, 0);
            this.TablaDistribucion.Controls.Add(this.TituloError, 1, 0);
            this.TablaDistribucion.Controls.Add(this.PanelDetalles, 0, 1);
            this.TablaDistribucion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TablaDistribucion.Location = new System.Drawing.Point(0, 0);
            this.TablaDistribucion.Name = "TablaDistribucion";
            this.TablaDistribucion.RowCount = 2;
            this.TablaDistribucion.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 46.77419F));
            this.TablaDistribucion.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 53.22581F));
            this.TablaDistribucion.Size = new System.Drawing.Size(272, 186);
            this.TablaDistribucion.TabIndex = 0;
            // 
            // IconoError
            // 
            this.IconoError.Dock = System.Windows.Forms.DockStyle.Fill;
            this.IconoError.Image = global::Jardineria.Properties.Resources.Exclamation_32xLG;
            this.IconoError.Location = new System.Drawing.Point(3, 3);
            this.IconoError.Name = "IconoError";
            this.IconoError.Size = new System.Drawing.Size(95, 80);
            this.IconoError.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.IconoError.TabIndex = 0;
            this.IconoError.TabStop = false;
            // 
            // TituloError
            // 
            this.TituloError.AutoSize = true;
            this.TituloError.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TituloError.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TituloError.Location = new System.Drawing.Point(104, 0);
            this.TituloError.Name = "TituloError";
            this.TituloError.Size = new System.Drawing.Size(165, 86);
            this.TituloError.TabIndex = 1;
            this.TituloError.Text = "Error";
            this.TituloError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PanelDetalles
            // 
            this.TablaDistribucion.SetColumnSpan(this.PanelDetalles, 2);
            this.PanelDetalles.Controls.Add(this.BotonDesplegar);
            this.PanelDetalles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelDetalles.Location = new System.Drawing.Point(3, 89);
            this.PanelDetalles.Name = "PanelDetalles";
            this.PanelDetalles.Size = new System.Drawing.Size(266, 94);
            this.PanelDetalles.TabIndex = 2;
            // 
            // BotonDesplegar
            // 
            this.BotonDesplegar.AutoSize = true;
            this.BotonDesplegar.FlatAppearance.BorderSize = 0;
            this.BotonDesplegar.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BotonDesplegar.Location = new System.Drawing.Point(81, 38);
            this.BotonDesplegar.Name = "BotonDesplegar";
            this.BotonDesplegar.Size = new System.Drawing.Size(93, 23);
            this.BotonDesplegar.TabIndex = 0;
            this.BotonDesplegar.Text = "Ver detalles";
            this.BotonDesplegar.UseVisualStyleBackColor = true;
            this.BotonDesplegar.Click += new System.EventHandler(this.BotonDesplegar_Click);
            // 
            // VentanaError
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(272, 186);
            this.Controls.Add(this.TablaDistribucion);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VentanaError";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Error en la aplicación";
            this.TopMost = true;
            this.TablaDistribucion.ResumeLayout(false);
            this.TablaDistribucion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IconoError)).EndInit();
            this.PanelDetalles.ResumeLayout(false);
            this.PanelDetalles.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel TablaDistribucion;
        private System.Windows.Forms.PictureBox IconoError;
        private System.Windows.Forms.Label TituloError;
        private System.Windows.Forms.Panel PanelDetalles;
        private System.Windows.Forms.Button BotonDesplegar;
    }
}