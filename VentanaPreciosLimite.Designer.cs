﻿namespace Jardineria
{
    partial class VentanaPreciosLimite
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VentanaPreciosLimite));
            this.LabelProductoMax = new System.Windows.Forms.Label();
            this.LabelProductoMin = new System.Windows.Forms.Label();
            this.LabelMasCaro = new System.Windows.Forms.Label();
            this.LabelMasBarato = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LabelProductoMax
            // 
            this.LabelProductoMax.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelProductoMax.Location = new System.Drawing.Point(60, 40);
            this.LabelProductoMax.Name = "LabelProductoMax";
            this.LabelProductoMax.Size = new System.Drawing.Size(286, 27);
            this.LabelProductoMax.TabIndex = 0;
            this.LabelProductoMax.Text = "label1";
            this.LabelProductoMax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LabelProductoMin
            // 
            this.LabelProductoMin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelProductoMin.Location = new System.Drawing.Point(60, 111);
            this.LabelProductoMin.Name = "LabelProductoMin";
            this.LabelProductoMin.Size = new System.Drawing.Size(286, 27);
            this.LabelProductoMin.TabIndex = 1;
            this.LabelProductoMin.Text = "label2";
            this.LabelProductoMin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LabelMasCaro
            // 
            this.LabelMasCaro.AutoSize = true;
            this.LabelMasCaro.Font = new System.Drawing.Font("Calibri Light", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelMasCaro.Location = new System.Drawing.Point(12, 17);
            this.LabelMasCaro.Name = "LabelMasCaro";
            this.LabelMasCaro.Size = new System.Drawing.Size(109, 15);
            this.LabelMasCaro.TabIndex = 2;
            this.LabelMasCaro.Text = "Producto más caro:";
            // 
            // LabelMasBarato
            // 
            this.LabelMasBarato.AutoSize = true;
            this.LabelMasBarato.Font = new System.Drawing.Font("Calibri Light", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelMasBarato.Location = new System.Drawing.Point(12, 90);
            this.LabelMasBarato.Name = "LabelMasBarato";
            this.LabelMasBarato.Size = new System.Drawing.Size(120, 15);
            this.LabelMasBarato.TabIndex = 3;
            this.LabelMasBarato.Text = "Producto más barato:";
            // 
            // VentanaPreciosLimite
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(407, 170);
            this.Controls.Add(this.LabelMasBarato);
            this.Controls.Add(this.LabelMasCaro);
            this.Controls.Add(this.LabelProductoMin);
            this.Controls.Add(this.LabelProductoMax);
            this.Font = new System.Drawing.Font("Calibri Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "VentanaPreciosLimite";
            this.Text = "Precios máximo y mínimo: Jardinería";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelProductoMax;
        private System.Windows.Forms.Label LabelProductoMin;
        private System.Windows.Forms.Label LabelMasCaro;
        private System.Windows.Forms.Label LabelMasBarato;
    }
}