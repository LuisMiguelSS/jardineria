﻿using Jardineria.Modelos.gui;

namespace Jardineria
{
    partial class VentanaPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VentanaPrincipal));
            this.BarraMenu = new System.Windows.Forms.MenuStrip();
            this.MenuInicio = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuInicioDesconectar = new System.Windows.Forms.ToolStripMenuItem();
            this.productosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.másCaroYMásBaratoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PanelTabulado = new System.Windows.Forms.TabControl();
            this.TabProductos = new System.Windows.Forms.TabPage();
            this.PanelFormularioProductos = new System.Windows.Forms.Panel();
            this.LabelAnadirProducto = new System.Windows.Forms.Label();
            this.NumericPrecioProveedor = new System.Windows.Forms.NumericUpDown();
            this.NumericPVP = new System.Windows.Forms.NumericUpDown();
            this.NumericStock = new System.Windows.Forms.NumericUpDown();
            this.ComboBoxGama = new System.Windows.Forms.ComboBox();
            this.TextBoxDescripcion = new System.Windows.Forms.TextBox();
            this.TextBoxProveedor = new System.Windows.Forms.TextBox();
            this.TextBoxDimensiones = new System.Windows.Forms.TextBox();
            this.TextBoxNombre = new System.Windows.Forms.TextBox();
            this.TextBoxIdentificador = new System.Windows.Forms.TextBox();
            this.ButtonLimpiar = new System.Windows.Forms.Button();
            this.ButtonAnadir = new System.Windows.Forms.Button();
            this.LabelPrecioProveedor = new System.Windows.Forms.Label();
            this.LabelPVP = new System.Windows.Forms.Label();
            this.LabelStock = new System.Windows.Forms.Label();
            this.LabelDescripcion = new System.Windows.Forms.Label();
            this.LabelProveedor = new System.Windows.Forms.Label();
            this.LabelDimensiones = new System.Windows.Forms.Label();
            this.LabelGama = new System.Windows.Forms.Label();
            this.LabelNombre = new System.Windows.Forms.Label();
            this.LabelIdentificador = new System.Windows.Forms.Label();
            this.ListaProductos = new Jardineria.Modelos.gui.ResultList();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TabGamas = new System.Windows.Forms.TabPage();
            this.ButtonEliminarGama = new System.Windows.Forms.Button();
            this.ListaGamas = new Jardineria.Modelos.gui.ResultList();
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TabEntregas = new System.Windows.Forms.TabPage();
            this.ListaEntregas = new Jardineria.Modelos.gui.ResultList();
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PanelSeleccionFecha = new System.Windows.Forms.Panel();
            this.ComboBoxFechas = new System.Windows.Forms.ComboBox();
            this.LabelMes = new System.Windows.Forms.Label();
            this.TabEmpleados = new System.Windows.Forms.TabPage();
            this.ListaEmpleados = new Jardineria.Modelos.gui.ResultList();
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PanelBusqueda = new System.Windows.Forms.Panel();
            this.LabelJefes = new System.Windows.Forms.Label();
            this.ListaJefes = new System.Windows.Forms.ComboBox();
            this.TabDeudores = new System.Windows.Forms.TabPage();
            this.ListaDeudores = new Jardineria.Modelos.gui.ResultList();
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader25 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader26 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader27 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader28 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader29 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader30 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader31 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader32 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader33 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader34 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.IconosPanelTabulado = new System.Windows.Forms.ImageList(this.components);
            this.BarraEstadoInferior = new System.Windows.Forms.StatusStrip();
            this.EmptyLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.LabelNumeroRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.BarraMenu.SuspendLayout();
            this.PanelTabulado.SuspendLayout();
            this.TabProductos.SuspendLayout();
            this.PanelFormularioProductos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericPrecioProveedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericPVP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericStock)).BeginInit();
            this.TabGamas.SuspendLayout();
            this.TabEntregas.SuspendLayout();
            this.PanelSeleccionFecha.SuspendLayout();
            this.TabEmpleados.SuspendLayout();
            this.PanelBusqueda.SuspendLayout();
            this.TabDeudores.SuspendLayout();
            this.BarraEstadoInferior.SuspendLayout();
            this.SuspendLayout();
            // 
            // BarraMenu
            // 
            this.BarraMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuInicio,
            this.productosToolStripMenuItem});
            this.BarraMenu.Location = new System.Drawing.Point(0, 0);
            this.BarraMenu.Name = "BarraMenu";
            this.BarraMenu.Size = new System.Drawing.Size(800, 24);
            this.BarraMenu.TabIndex = 0;
            this.BarraMenu.Text = "menuStrip1";
            // 
            // MenuInicio
            // 
            this.MenuInicio.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuInicioDesconectar});
            this.MenuInicio.Name = "MenuInicio";
            this.MenuInicio.Size = new System.Drawing.Size(48, 20);
            this.MenuInicio.Text = "Inicio";
            // 
            // MenuInicioDesconectar
            // 
            this.MenuInicioDesconectar.Image = global::Jardineria.Properties.Resources.Exit_16x;
            this.MenuInicioDesconectar.Name = "MenuInicioDesconectar";
            this.MenuInicioDesconectar.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.MenuInicioDesconectar.Size = new System.Drawing.Size(181, 22);
            this.MenuInicioDesconectar.Text = "Desconectar";
            this.MenuInicioDesconectar.Click += new System.EventHandler(this.MenuInicioDesconectar_Click);
            // 
            // productosToolStripMenuItem
            // 
            this.productosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.másCaroYMásBaratoToolStripMenuItem});
            this.productosToolStripMenuItem.Name = "productosToolStripMenuItem";
            this.productosToolStripMenuItem.Size = new System.Drawing.Size(82, 20);
            this.productosToolStripMenuItem.Text = "Productos...";
            // 
            // másCaroYMásBaratoToolStripMenuItem
            // 
            this.másCaroYMásBaratoToolStripMenuItem.Name = "másCaroYMásBaratoToolStripMenuItem";
            this.másCaroYMásBaratoToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.másCaroYMásBaratoToolStripMenuItem.Text = "Más caro y más barato";
            this.másCaroYMásBaratoToolStripMenuItem.Click += new System.EventHandler(this.másCaroYMásBaratoToolStripMenuItem_Click);
            // 
            // PanelTabulado
            // 
            this.PanelTabulado.Controls.Add(this.TabProductos);
            this.PanelTabulado.Controls.Add(this.TabGamas);
            this.PanelTabulado.Controls.Add(this.TabEntregas);
            this.PanelTabulado.Controls.Add(this.TabEmpleados);
            this.PanelTabulado.Controls.Add(this.TabDeudores);
            this.PanelTabulado.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelTabulado.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PanelTabulado.ImageList = this.IconosPanelTabulado;
            this.PanelTabulado.Location = new System.Drawing.Point(0, 24);
            this.PanelTabulado.Multiline = true;
            this.PanelTabulado.Name = "PanelTabulado";
            this.PanelTabulado.SelectedIndex = 0;
            this.PanelTabulado.Size = new System.Drawing.Size(800, 412);
            this.PanelTabulado.TabIndex = 1;
            this.PanelTabulado.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.PanelTabulado_Selecting);
            // 
            // TabProductos
            // 
            this.TabProductos.Controls.Add(this.PanelFormularioProductos);
            this.TabProductos.Controls.Add(this.ListaProductos);
            this.TabProductos.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TabProductos.ImageIndex = 4;
            this.TabProductos.Location = new System.Drawing.Point(4, 23);
            this.TabProductos.Name = "TabProductos";
            this.TabProductos.Padding = new System.Windows.Forms.Padding(3);
            this.TabProductos.Size = new System.Drawing.Size(792, 385);
            this.TabProductos.TabIndex = 0;
            this.TabProductos.Text = "Productos";
            this.TabProductos.UseVisualStyleBackColor = true;
            // 
            // PanelFormularioProductos
            // 
            this.PanelFormularioProductos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.PanelFormularioProductos.Controls.Add(this.LabelAnadirProducto);
            this.PanelFormularioProductos.Controls.Add(this.NumericPrecioProveedor);
            this.PanelFormularioProductos.Controls.Add(this.NumericPVP);
            this.PanelFormularioProductos.Controls.Add(this.NumericStock);
            this.PanelFormularioProductos.Controls.Add(this.ComboBoxGama);
            this.PanelFormularioProductos.Controls.Add(this.TextBoxDescripcion);
            this.PanelFormularioProductos.Controls.Add(this.TextBoxProveedor);
            this.PanelFormularioProductos.Controls.Add(this.TextBoxDimensiones);
            this.PanelFormularioProductos.Controls.Add(this.TextBoxNombre);
            this.PanelFormularioProductos.Controls.Add(this.TextBoxIdentificador);
            this.PanelFormularioProductos.Controls.Add(this.ButtonLimpiar);
            this.PanelFormularioProductos.Controls.Add(this.ButtonAnadir);
            this.PanelFormularioProductos.Controls.Add(this.LabelPrecioProveedor);
            this.PanelFormularioProductos.Controls.Add(this.LabelPVP);
            this.PanelFormularioProductos.Controls.Add(this.LabelStock);
            this.PanelFormularioProductos.Controls.Add(this.LabelDescripcion);
            this.PanelFormularioProductos.Controls.Add(this.LabelProveedor);
            this.PanelFormularioProductos.Controls.Add(this.LabelDimensiones);
            this.PanelFormularioProductos.Controls.Add(this.LabelGama);
            this.PanelFormularioProductos.Controls.Add(this.LabelNombre);
            this.PanelFormularioProductos.Controls.Add(this.LabelIdentificador);
            this.PanelFormularioProductos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelFormularioProductos.Location = new System.Drawing.Point(486, 3);
            this.PanelFormularioProductos.Name = "PanelFormularioProductos";
            this.PanelFormularioProductos.Size = new System.Drawing.Size(303, 379);
            this.PanelFormularioProductos.TabIndex = 3;
            // 
            // LabelAnadirProducto
            // 
            this.LabelAnadirProducto.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelAnadirProducto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelAnadirProducto.Font = new System.Drawing.Font("Calibri Light", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelAnadirProducto.Location = new System.Drawing.Point(45, 8);
            this.LabelAnadirProducto.Name = "LabelAnadirProducto";
            this.LabelAnadirProducto.Size = new System.Drawing.Size(212, 30);
            this.LabelAnadirProducto.TabIndex = 34;
            this.LabelAnadirProducto.Text = "AÑADIR PRODUCTO";
            this.LabelAnadirProducto.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // NumericPrecioProveedor
            // 
            this.NumericPrecioProveedor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.NumericPrecioProveedor.DecimalPlaces = 2;
            this.NumericPrecioProveedor.Location = new System.Drawing.Point(147, 288);
            this.NumericPrecioProveedor.Maximum = new decimal(new int[] {
            -1530494977,
            232830,
            0,
            0});
            this.NumericPrecioProveedor.Name = "NumericPrecioProveedor";
            this.NumericPrecioProveedor.Size = new System.Drawing.Size(84, 22);
            this.NumericPrecioProveedor.TabIndex = 33;
            this.NumericPrecioProveedor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NumericPrecioProveedor.ThousandsSeparator = true;
            // 
            // NumericPVP
            // 
            this.NumericPVP.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.NumericPVP.DecimalPlaces = 2;
            this.NumericPVP.Location = new System.Drawing.Point(147, 258);
            this.NumericPVP.Maximum = new decimal(new int[] {
            -1530494977,
            232830,
            0,
            0});
            this.NumericPVP.Name = "NumericPVP";
            this.NumericPVP.Size = new System.Drawing.Size(84, 22);
            this.NumericPVP.TabIndex = 32;
            this.NumericPVP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NumericPVP.ThousandsSeparator = true;
            // 
            // NumericStock
            // 
            this.NumericStock.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.NumericStock.Location = new System.Drawing.Point(147, 228);
            this.NumericStock.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.NumericStock.Name = "NumericStock";
            this.NumericStock.Size = new System.Drawing.Size(72, 22);
            this.NumericStock.TabIndex = 31;
            this.NumericStock.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NumericStock.ThousandsSeparator = true;
            // 
            // ComboBoxGama
            // 
            this.ComboBoxGama.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ComboBoxGama.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxGama.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ComboBoxGama.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComboBoxGama.FormattingEnabled = true;
            this.ComboBoxGama.Location = new System.Drawing.Point(147, 107);
            this.ComboBoxGama.Name = "ComboBoxGama";
            this.ComboBoxGama.Size = new System.Drawing.Size(121, 23);
            this.ComboBoxGama.TabIndex = 30;
            // 
            // TextBoxDescripcion
            // 
            this.TextBoxDescripcion.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TextBoxDescripcion.Location = new System.Drawing.Point(147, 197);
            this.TextBoxDescripcion.Name = "TextBoxDescripcion";
            this.TextBoxDescripcion.Size = new System.Drawing.Size(146, 22);
            this.TextBoxDescripcion.TabIndex = 29;
            // 
            // TextBoxProveedor
            // 
            this.TextBoxProveedor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TextBoxProveedor.Location = new System.Drawing.Point(147, 167);
            this.TextBoxProveedor.Name = "TextBoxProveedor";
            this.TextBoxProveedor.Size = new System.Drawing.Size(121, 22);
            this.TextBoxProveedor.TabIndex = 28;
            // 
            // TextBoxDimensiones
            // 
            this.TextBoxDimensiones.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TextBoxDimensiones.Location = new System.Drawing.Point(147, 137);
            this.TextBoxDimensiones.Name = "TextBoxDimensiones";
            this.TextBoxDimensiones.Size = new System.Drawing.Size(86, 22);
            this.TextBoxDimensiones.TabIndex = 27;
            // 
            // TextBoxNombre
            // 
            this.TextBoxNombre.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TextBoxNombre.Location = new System.Drawing.Point(147, 77);
            this.TextBoxNombre.Name = "TextBoxNombre";
            this.TextBoxNombre.Size = new System.Drawing.Size(136, 22);
            this.TextBoxNombre.TabIndex = 26;
            // 
            // TextBoxIdentificador
            // 
            this.TextBoxIdentificador.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TextBoxIdentificador.Location = new System.Drawing.Point(147, 47);
            this.TextBoxIdentificador.Name = "TextBoxIdentificador";
            this.TextBoxIdentificador.Size = new System.Drawing.Size(100, 22);
            this.TextBoxIdentificador.TabIndex = 25;
            // 
            // ButtonLimpiar
            // 
            this.ButtonLimpiar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ButtonLimpiar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ButtonLimpiar.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonLimpiar.Location = new System.Drawing.Point(157, 325);
            this.ButtonLimpiar.Name = "ButtonLimpiar";
            this.ButtonLimpiar.Size = new System.Drawing.Size(74, 34);
            this.ButtonLimpiar.TabIndex = 24;
            this.ButtonLimpiar.Text = "LIMPIAR";
            this.ButtonLimpiar.UseVisualStyleBackColor = true;
            this.ButtonLimpiar.Click += new System.EventHandler(this.ButtonLimpiar_Click);
            // 
            // ButtonAnadir
            // 
            this.ButtonAnadir.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ButtonAnadir.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ButtonAnadir.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonAnadir.Location = new System.Drawing.Point(59, 325);
            this.ButtonAnadir.Name = "ButtonAnadir";
            this.ButtonAnadir.Size = new System.Drawing.Size(74, 34);
            this.ButtonAnadir.TabIndex = 23;
            this.ButtonAnadir.Text = "AÑADIR";
            this.ButtonAnadir.UseVisualStyleBackColor = true;
            this.ButtonAnadir.Click += new System.EventHandler(this.ButtonAnadir_Click);
            // 
            // LabelPrecioProveedor
            // 
            this.LabelPrecioProveedor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelPrecioProveedor.AutoSize = true;
            this.LabelPrecioProveedor.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelPrecioProveedor.Location = new System.Drawing.Point(10, 286);
            this.LabelPrecioProveedor.Name = "LabelPrecioProveedor";
            this.LabelPrecioProveedor.Size = new System.Drawing.Size(122, 19);
            this.LabelPrecioProveedor.TabIndex = 22;
            this.LabelPrecioProveedor.Text = "Precio Proveedor:";
            // 
            // LabelPVP
            // 
            this.LabelPVP.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelPVP.AutoSize = true;
            this.LabelPVP.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelPVP.Location = new System.Drawing.Point(10, 256);
            this.LabelPVP.Name = "LabelPVP";
            this.LabelPVP.Size = new System.Drawing.Size(43, 19);
            this.LabelPVP.TabIndex = 21;
            this.LabelPVP.Text = "P.V.P.:";
            // 
            // LabelStock
            // 
            this.LabelStock.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelStock.AutoSize = true;
            this.LabelStock.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelStock.Location = new System.Drawing.Point(10, 226);
            this.LabelStock.Name = "LabelStock";
            this.LabelStock.Size = new System.Drawing.Size(47, 19);
            this.LabelStock.TabIndex = 20;
            this.LabelStock.Text = "Stock:";
            // 
            // LabelDescripcion
            // 
            this.LabelDescripcion.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelDescripcion.AutoSize = true;
            this.LabelDescripcion.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelDescripcion.Location = new System.Drawing.Point(10, 196);
            this.LabelDescripcion.Name = "LabelDescripcion";
            this.LabelDescripcion.Size = new System.Drawing.Size(89, 19);
            this.LabelDescripcion.TabIndex = 19;
            this.LabelDescripcion.Text = "Descripción:";
            // 
            // LabelProveedor
            // 
            this.LabelProveedor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelProveedor.AutoSize = true;
            this.LabelProveedor.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelProveedor.Location = new System.Drawing.Point(10, 166);
            this.LabelProveedor.Name = "LabelProveedor";
            this.LabelProveedor.Size = new System.Drawing.Size(78, 19);
            this.LabelProveedor.TabIndex = 18;
            this.LabelProveedor.Text = "Proveedor:";
            // 
            // LabelDimensiones
            // 
            this.LabelDimensiones.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelDimensiones.AutoSize = true;
            this.LabelDimensiones.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelDimensiones.Location = new System.Drawing.Point(10, 136);
            this.LabelDimensiones.Name = "LabelDimensiones";
            this.LabelDimensiones.Size = new System.Drawing.Size(97, 19);
            this.LabelDimensiones.TabIndex = 17;
            this.LabelDimensiones.Text = "Dimensiones:";
            // 
            // LabelGama
            // 
            this.LabelGama.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelGama.AutoSize = true;
            this.LabelGama.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelGama.Location = new System.Drawing.Point(10, 106);
            this.LabelGama.Name = "LabelGama";
            this.LabelGama.Size = new System.Drawing.Size(51, 19);
            this.LabelGama.TabIndex = 16;
            this.LabelGama.Text = "Gama:";
            // 
            // LabelNombre
            // 
            this.LabelNombre.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelNombre.AutoSize = true;
            this.LabelNombre.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelNombre.Location = new System.Drawing.Point(10, 76);
            this.LabelNombre.Name = "LabelNombre";
            this.LabelNombre.Size = new System.Drawing.Size(64, 19);
            this.LabelNombre.TabIndex = 15;
            this.LabelNombre.Text = "Nombre:";
            // 
            // LabelIdentificador
            // 
            this.LabelIdentificador.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelIdentificador.AutoSize = true;
            this.LabelIdentificador.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelIdentificador.Location = new System.Drawing.Point(10, 46);
            this.LabelIdentificador.Name = "LabelIdentificador";
            this.LabelIdentificador.Size = new System.Drawing.Size(94, 19);
            this.LabelIdentificador.TabIndex = 14;
            this.LabelIdentificador.Text = "Identificador:";
            // 
            // ListaProductos
            // 
            this.ListaProductos.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6});
            this.ListaProductos.Dock = System.Windows.Forms.DockStyle.Left;
            this.ListaProductos.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListaProductos.FullRowSelect = true;
            this.ListaProductos.GridLines = true;
            this.ListaProductos.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.ListaProductos.HideSelection = false;
            this.ListaProductos.Location = new System.Drawing.Point(3, 3);
            this.ListaProductos.MultiSelect = false;
            this.ListaProductos.Name = "ListaProductos";
            this.ListaProductos.Size = new System.Drawing.Size(483, 379);
            this.ListaProductos.TabIndex = 2;
            this.ListaProductos.UseCompatibleStateImageBehavior = false;
            this.ListaProductos.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "";
            this.columnHeader1.Width = 25;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Código Producto";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader2.Width = 114;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Nombre";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader3.Width = 63;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Gama";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader4.Width = 47;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Unidades Disponibles";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader5.Width = 146;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Precio Unitario";
            this.columnHeader6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader6.Width = 391;
            // 
            // TabGamas
            // 
            this.TabGamas.Controls.Add(this.ButtonEliminarGama);
            this.TabGamas.Controls.Add(this.ListaGamas);
            this.TabGamas.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TabGamas.ImageIndex = 2;
            this.TabGamas.Location = new System.Drawing.Point(4, 23);
            this.TabGamas.Name = "TabGamas";
            this.TabGamas.Padding = new System.Windows.Forms.Padding(3);
            this.TabGamas.Size = new System.Drawing.Size(792, 385);
            this.TabGamas.TabIndex = 1;
            this.TabGamas.Text = "Gamas";
            this.TabGamas.UseVisualStyleBackColor = true;
            // 
            // ButtonEliminarGama
            // 
            this.ButtonEliminarGama.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonEliminarGama.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(82)))), ((int)(((byte)(82)))));
            this.ButtonEliminarGama.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonEliminarGama.Font = new System.Drawing.Font("Calibri Light", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonEliminarGama.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.ButtonEliminarGama.Image = global::Jardineria.Properties.Resources.trash_icon_18_16;
            this.ButtonEliminarGama.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonEliminarGama.Location = new System.Drawing.Point(701, 349);
            this.ButtonEliminarGama.Name = "ButtonEliminarGama";
            this.ButtonEliminarGama.Size = new System.Drawing.Size(85, 30);
            this.ButtonEliminarGama.TabIndex = 4;
            this.ButtonEliminarGama.Text = "Eliminar";
            this.ButtonEliminarGama.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ButtonEliminarGama.UseVisualStyleBackColor = false;
            this.ButtonEliminarGama.Click += new System.EventHandler(this.ButtonEliminarGama_Click);
            // 
            // ListaGamas
            // 
            this.ListaGamas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ListaGamas.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9});
            this.ListaGamas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListaGamas.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListaGamas.FullRowSelect = true;
            this.ListaGamas.GridLines = true;
            this.ListaGamas.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.ListaGamas.HideSelection = false;
            this.ListaGamas.Location = new System.Drawing.Point(3, 3);
            this.ListaGamas.MultiSelect = false;
            this.ListaGamas.Name = "ListaGamas";
            this.ListaGamas.Size = new System.Drawing.Size(786, 379);
            this.ListaGamas.TabIndex = 3;
            this.ListaGamas.UseCompatibleStateImageBehavior = false;
            this.ListaGamas.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "";
            this.columnHeader7.Width = 25;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Nombre";
            this.columnHeader8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader8.Width = 63;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Descripción";
            this.columnHeader9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader9.Width = 698;
            // 
            // TabEntregas
            // 
            this.TabEntregas.Controls.Add(this.ListaEntregas);
            this.TabEntregas.Controls.Add(this.PanelSeleccionFecha);
            this.TabEntregas.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TabEntregas.ImageIndex = 1;
            this.TabEntregas.Location = new System.Drawing.Point(4, 23);
            this.TabEntregas.Name = "TabEntregas";
            this.TabEntregas.Size = new System.Drawing.Size(792, 385);
            this.TabEntregas.TabIndex = 2;
            this.TabEntregas.Text = "Entregas";
            this.TabEntregas.UseVisualStyleBackColor = true;
            // 
            // ListaEntregas
            // 
            this.ListaEntregas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ListaEntregas.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader10,
            this.columnHeader11,
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader16});
            this.ListaEntregas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListaEntregas.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListaEntregas.FullRowSelect = true;
            this.ListaEntregas.GridLines = true;
            this.ListaEntregas.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.ListaEntregas.HideSelection = false;
            this.ListaEntregas.Location = new System.Drawing.Point(0, 48);
            this.ListaEntregas.MultiSelect = false;
            this.ListaEntregas.Name = "ListaEntregas";
            this.ListaEntregas.Size = new System.Drawing.Size(792, 337);
            this.ListaEntregas.TabIndex = 4;
            this.ListaEntregas.UseCompatibleStateImageBehavior = false;
            this.ListaEntregas.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "";
            this.columnHeader10.Width = 25;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Identificador";
            this.columnHeader11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader11.Width = 92;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Fecha Realización";
            this.columnHeader12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader12.Width = 121;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Fecha Esperada";
            this.columnHeader13.Width = 107;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "FechaEntrega";
            this.columnHeader14.Width = 95;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Comentarios";
            this.columnHeader15.Width = 91;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "Código Cliente";
            this.columnHeader16.Width = 261;
            // 
            // PanelSeleccionFecha
            // 
            this.PanelSeleccionFecha.Controls.Add(this.ComboBoxFechas);
            this.PanelSeleccionFecha.Controls.Add(this.LabelMes);
            this.PanelSeleccionFecha.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelSeleccionFecha.Location = new System.Drawing.Point(0, 0);
            this.PanelSeleccionFecha.Name = "PanelSeleccionFecha";
            this.PanelSeleccionFecha.Size = new System.Drawing.Size(792, 48);
            this.PanelSeleccionFecha.TabIndex = 5;
            // 
            // ComboBoxFechas
            // 
            this.ComboBoxFechas.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ComboBoxFechas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxFechas.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ComboBoxFechas.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComboBoxFechas.FormattingEnabled = true;
            this.ComboBoxFechas.Items.AddRange(new object[] {
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octube",
            "Noviembre",
            "Diciembre"});
            this.ComboBoxFechas.Location = new System.Drawing.Point(350, 13);
            this.ComboBoxFechas.Name = "ComboBoxFechas";
            this.ComboBoxFechas.Size = new System.Drawing.Size(131, 22);
            this.ComboBoxFechas.TabIndex = 1;
            this.ComboBoxFechas.DropDownClosed += new System.EventHandler(this.ComboBoxFechas_DropDownClosed);
            // 
            // LabelMes
            // 
            this.LabelMes.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelMes.AutoSize = true;
            this.LabelMes.Location = new System.Drawing.Point(311, 16);
            this.LabelMes.Name = "LabelMes";
            this.LabelMes.Size = new System.Drawing.Size(33, 14);
            this.LabelMes.TabIndex = 0;
            this.LabelMes.Text = "Mes:";
            // 
            // TabEmpleados
            // 
            this.TabEmpleados.Controls.Add(this.ListaEmpleados);
            this.TabEmpleados.Controls.Add(this.PanelBusqueda);
            this.TabEmpleados.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TabEmpleados.ImageIndex = 0;
            this.TabEmpleados.Location = new System.Drawing.Point(4, 23);
            this.TabEmpleados.Name = "TabEmpleados";
            this.TabEmpleados.Size = new System.Drawing.Size(792, 385);
            this.TabEmpleados.TabIndex = 3;
            this.TabEmpleados.Text = "Empleados";
            this.TabEmpleados.UseVisualStyleBackColor = true;
            // 
            // ListaEmpleados
            // 
            this.ListaEmpleados.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ListaEmpleados.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader17,
            this.columnHeader18,
            this.columnHeader19,
            this.columnHeader20});
            this.ListaEmpleados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListaEmpleados.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListaEmpleados.FullRowSelect = true;
            this.ListaEmpleados.GridLines = true;
            this.ListaEmpleados.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.ListaEmpleados.HideSelection = false;
            this.ListaEmpleados.Location = new System.Drawing.Point(0, 48);
            this.ListaEmpleados.MultiSelect = false;
            this.ListaEmpleados.Name = "ListaEmpleados";
            this.ListaEmpleados.Size = new System.Drawing.Size(792, 337);
            this.ListaEmpleados.TabIndex = 5;
            this.ListaEmpleados.UseCompatibleStateImageBehavior = false;
            this.ListaEmpleados.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "";
            this.columnHeader17.Width = 25;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "Nombre";
            this.columnHeader18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader18.Width = 63;
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "Apellidos";
            this.columnHeader19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader19.Width = 71;
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "Email";
            this.columnHeader20.Width = 633;
            // 
            // PanelBusqueda
            // 
            this.PanelBusqueda.Controls.Add(this.LabelJefes);
            this.PanelBusqueda.Controls.Add(this.ListaJefes);
            this.PanelBusqueda.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelBusqueda.Location = new System.Drawing.Point(0, 0);
            this.PanelBusqueda.Name = "PanelBusqueda";
            this.PanelBusqueda.Size = new System.Drawing.Size(792, 48);
            this.PanelBusqueda.TabIndex = 0;
            // 
            // LabelJefes
            // 
            this.LabelJefes.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelJefes.AutoSize = true;
            this.LabelJefes.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelJefes.Location = new System.Drawing.Point(246, 16);
            this.LabelJefes.Name = "LabelJefes";
            this.LabelJefes.Size = new System.Drawing.Size(108, 14);
            this.LabelJefes.TabIndex = 1;
            this.LabelJefes.Text = "Jefes Disponibles:";
            // 
            // ListaJefes
            // 
            this.ListaJefes.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ListaJefes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ListaJefes.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ListaJefes.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListaJefes.FormattingEnabled = true;
            this.ListaJefes.Location = new System.Drawing.Point(369, 13);
            this.ListaJefes.Name = "ListaJefes";
            this.ListaJefes.Size = new System.Drawing.Size(178, 22);
            this.ListaJefes.TabIndex = 0;
            this.ListaJefes.DropDownClosed += new System.EventHandler(this.ListaJefes_DropDownClosed);
            // 
            // TabDeudores
            // 
            this.TabDeudores.Controls.Add(this.ListaDeudores);
            this.TabDeudores.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TabDeudores.ImageIndex = 3;
            this.TabDeudores.Location = new System.Drawing.Point(4, 23);
            this.TabDeudores.Name = "TabDeudores";
            this.TabDeudores.Size = new System.Drawing.Size(792, 385);
            this.TabDeudores.TabIndex = 4;
            this.TabDeudores.Text = "Deudores";
            this.TabDeudores.UseVisualStyleBackColor = true;
            // 
            // ListaDeudores
            // 
            this.ListaDeudores.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ListaDeudores.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader21,
            this.columnHeader22,
            this.columnHeader23,
            this.columnHeader24,
            this.columnHeader25,
            this.columnHeader26,
            this.columnHeader27,
            this.columnHeader28,
            this.columnHeader29,
            this.columnHeader30,
            this.columnHeader31,
            this.columnHeader32,
            this.columnHeader33,
            this.columnHeader34});
            this.ListaDeudores.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListaDeudores.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListaDeudores.FullRowSelect = true;
            this.ListaDeudores.GridLines = true;
            this.ListaDeudores.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.ListaDeudores.HideSelection = false;
            this.ListaDeudores.Location = new System.Drawing.Point(0, 0);
            this.ListaDeudores.MultiSelect = false;
            this.ListaDeudores.Name = "ListaDeudores";
            this.ListaDeudores.Size = new System.Drawing.Size(792, 385);
            this.ListaDeudores.TabIndex = 5;
            this.ListaDeudores.UseCompatibleStateImageBehavior = false;
            this.ListaDeudores.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "";
            this.columnHeader21.Width = 25;
            // 
            // columnHeader22
            // 
            this.columnHeader22.Text = "Identificador";
            this.columnHeader22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader22.Width = 92;
            // 
            // columnHeader23
            // 
            this.columnHeader23.Text = "Nombre";
            this.columnHeader23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader23.Width = 63;
            // 
            // columnHeader24
            // 
            this.columnHeader24.Text = "Nombre Contacto";
            this.columnHeader24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader24.Width = 121;
            // 
            // columnHeader25
            // 
            this.columnHeader25.Text = "Telefono";
            this.columnHeader25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader25.Width = 68;
            // 
            // columnHeader26
            // 
            this.columnHeader26.Text = "Fax";
            this.columnHeader26.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader26.Width = 33;
            // 
            // columnHeader27
            // 
            this.columnHeader27.Text = "Dirección Principal";
            this.columnHeader27.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader27.Width = 127;
            // 
            // columnHeader28
            // 
            this.columnHeader28.Text = "Dirección Alternativa";
            this.columnHeader28.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader28.Width = 142;
            // 
            // columnHeader29
            // 
            this.columnHeader29.Text = "Ciudad";
            this.columnHeader29.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader29.Width = 55;
            // 
            // columnHeader30
            // 
            this.columnHeader30.Text = "Región";
            this.columnHeader30.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader30.Width = 55;
            // 
            // columnHeader31
            // 
            this.columnHeader31.Text = "País";
            this.columnHeader31.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader31.Width = 36;
            // 
            // columnHeader32
            // 
            this.columnHeader32.Text = "Código Postal";
            this.columnHeader32.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader32.Width = 96;
            // 
            // columnHeader33
            // 
            this.columnHeader33.Text = "Representante";
            this.columnHeader33.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader33.Width = 104;
            // 
            // columnHeader34
            // 
            this.columnHeader34.Text = "Límite Crédito";
            this.columnHeader34.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader34.Width = 99;
            // 
            // IconosPanelTabulado
            // 
            this.IconosPanelTabulado.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("IconosPanelTabulado.ImageStream")));
            this.IconosPanelTabulado.TransparentColor = System.Drawing.Color.Transparent;
            this.IconosPanelTabulado.Images.SetKeyName(0, "Employee");
            this.IconosPanelTabulado.Images.SetKeyName(1, "Received");
            this.IconosPanelTabulado.Images.SetKeyName(2, "Categories");
            this.IconosPanelTabulado.Images.SetKeyName(3, "Currency");
            this.IconosPanelTabulado.Images.SetKeyName(4, "ShoppingBag");
            // 
            // BarraEstadoInferior
            // 
            this.BarraEstadoInferior.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.EmptyLabel,
            this.LabelNumeroRegistros});
            this.BarraEstadoInferior.Location = new System.Drawing.Point(0, 436);
            this.BarraEstadoInferior.Name = "BarraEstadoInferior";
            this.BarraEstadoInferior.Size = new System.Drawing.Size(800, 22);
            this.BarraEstadoInferior.TabIndex = 4;
            this.BarraEstadoInferior.Text = "statusStrip1";
            // 
            // EmptyLabel
            // 
            this.EmptyLabel.Name = "EmptyLabel";
            this.EmptyLabel.Size = new System.Drawing.Size(692, 17);
            this.EmptyLabel.Spring = true;
            // 
            // LabelNumeroRegistros
            // 
            this.LabelNumeroRegistros.Name = "LabelNumeroRegistros";
            this.LabelNumeroRegistros.Size = new System.Drawing.Size(93, 17);
            this.LabelNumeroRegistros.Text = "Nº resultados: ...";
            // 
            // VentanaPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 458);
            this.Controls.Add(this.PanelTabulado);
            this.Controls.Add(this.BarraEstadoInferior);
            this.Controls.Add(this.BarraMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.BarraMenu;
            this.MinimumSize = new System.Drawing.Size(816, 497);
            this.Name = "VentanaPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Inicio: Jardinería";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VentanaPrincipal_FormClosing);
            this.BarraMenu.ResumeLayout(false);
            this.BarraMenu.PerformLayout();
            this.PanelTabulado.ResumeLayout(false);
            this.TabProductos.ResumeLayout(false);
            this.PanelFormularioProductos.ResumeLayout(false);
            this.PanelFormularioProductos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericPrecioProveedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericPVP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericStock)).EndInit();
            this.TabGamas.ResumeLayout(false);
            this.TabEntregas.ResumeLayout(false);
            this.PanelSeleccionFecha.ResumeLayout(false);
            this.PanelSeleccionFecha.PerformLayout();
            this.TabEmpleados.ResumeLayout(false);
            this.PanelBusqueda.ResumeLayout(false);
            this.PanelBusqueda.PerformLayout();
            this.TabDeudores.ResumeLayout(false);
            this.BarraEstadoInferior.ResumeLayout(false);
            this.BarraEstadoInferior.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip BarraMenu;
        private System.Windows.Forms.ToolStripMenuItem MenuInicio;
        private System.Windows.Forms.ToolStripMenuItem MenuInicioDesconectar;
        private System.Windows.Forms.TabControl PanelTabulado;
        private System.Windows.Forms.TabPage TabProductos;
        private System.Windows.Forms.TabPage TabGamas;
        private System.Windows.Forms.ImageList IconosPanelTabulado;
        private ResultList ListaProductos;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.StatusStrip BarraEstadoInferior;
        private System.Windows.Forms.ToolStripStatusLabel LabelNumeroRegistros;
        private System.Windows.Forms.ToolStripStatusLabel EmptyLabel;
        private System.Windows.Forms.TabPage TabEntregas;
        private System.Windows.Forms.TabPage TabEmpleados;
        private System.Windows.Forms.TabPage TabDeudores;
        private ResultList ListaGamas;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private ResultList ListaEntregas;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.Panel PanelBusqueda;
        private ResultList ListaEmpleados;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.Label LabelJefes;
        private System.Windows.Forms.ComboBox ListaJefes;
        private ResultList ListaDeudores;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private System.Windows.Forms.ColumnHeader columnHeader27;
        private System.Windows.Forms.ColumnHeader columnHeader28;
        private System.Windows.Forms.ColumnHeader columnHeader29;
        private System.Windows.Forms.ColumnHeader columnHeader30;
        private System.Windows.Forms.ColumnHeader columnHeader31;
        private System.Windows.Forms.ColumnHeader columnHeader32;
        private System.Windows.Forms.ColumnHeader columnHeader33;
        private System.Windows.Forms.ColumnHeader columnHeader34;
        private System.Windows.Forms.Panel PanelFormularioProductos;
        private System.Windows.Forms.Button ButtonLimpiar;
        private System.Windows.Forms.Button ButtonAnadir;
        private System.Windows.Forms.Label LabelPrecioProveedor;
        private System.Windows.Forms.Label LabelPVP;
        private System.Windows.Forms.Label LabelStock;
        private System.Windows.Forms.Label LabelDescripcion;
        private System.Windows.Forms.Label LabelProveedor;
        private System.Windows.Forms.Label LabelDimensiones;
        private System.Windows.Forms.Label LabelGama;
        private System.Windows.Forms.Label LabelNombre;
        private System.Windows.Forms.Label LabelIdentificador;
        private System.Windows.Forms.NumericUpDown NumericPrecioProveedor;
        private System.Windows.Forms.NumericUpDown NumericPVP;
        private System.Windows.Forms.NumericUpDown NumericStock;
        private System.Windows.Forms.ComboBox ComboBoxGama;
        private System.Windows.Forms.TextBox TextBoxDescripcion;
        private System.Windows.Forms.TextBox TextBoxProveedor;
        private System.Windows.Forms.TextBox TextBoxDimensiones;
        private System.Windows.Forms.TextBox TextBoxNombre;
        private System.Windows.Forms.TextBox TextBoxIdentificador;
        private System.Windows.Forms.Label LabelAnadirProducto;
        private System.Windows.Forms.Button ButtonEliminarGama;
        private System.Windows.Forms.Panel PanelSeleccionFecha;
        private System.Windows.Forms.ComboBox ComboBoxFechas;
        private System.Windows.Forms.Label LabelMes;
        private System.Windows.Forms.ToolStripMenuItem productosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem másCaroYMásBaratoToolStripMenuItem;
    }
}

