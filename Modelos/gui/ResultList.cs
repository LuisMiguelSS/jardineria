﻿using System.Drawing;
using System.Windows.Forms;

namespace Jardineria.Modelos.gui
{
    class ResultList : ListView
    {
        //
        // Constructor
        //
        public ResultList() : base()
        {
            Font = new Font("Calibri", 11);

            View = View.Details;
            HeaderStyle = ColumnHeaderStyle.Nonclickable;
            MultiSelect = false;
            Scrollable = true;

            SetAlturaItems(30);
        }

        //
        // Otros métodos
        //
        public void SetAlturaItems(int altura)
        {
            ImageList listaImagenes = new ImageList()
            {
                ImageSize = new Size(1, altura)
            };

            SmallImageList = listaImagenes;
        }
    }
}
