﻿namespace Jardineria.Modelos
{
    public class Gama
    {
        //
        // Atributos
        //
        private string Nombre;
        private string Descripcion;
        private string DescripcionHtml;
        private string Imagen;

        //
        // Constructor
        //
        public Gama(string nombre = "Desconocido", string descripcion = "",
                    string html = "<p></p>",
                    string imagen = "")
        {
            Nombre = nombre;
            Descripcion = descripcion;
            DescripcionHtml = html;
            Imagen = imagen;
        }

        //
        // Getters
        //
        public string GetNombre() { return Nombre; }
        public string GetDescripcion() { return Descripcion; }
        public string GetDescripcionHTML() { return DescripcionHtml; }
        public string GetImagen() { return Imagen; }

        //
        // Otros métodos
        //
        public override string ToString()
        {
            return "Gama { nombre = " + Nombre + ", " +
                          "descripcion = " + Descripcion + ", " +
                          "html = " + DescripcionHtml + ", " +
                          "imagen = " + Imagen + "}";
        }

    }
}
