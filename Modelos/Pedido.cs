﻿using System;

namespace Jardineria.Modelos
{
    class Pedido
    {
        //
        // Atributos
        //
        private int Identificador;
        private DateTime? FechaRealizado;
        private DateTime? FechaEsperada;
        private DateTime? FechaEntrega;
        private string Estado;
        private string Comentarios;
        private int CodigoCliente;

        //
        // Constructor
        //
        public Pedido(int id = -1, DateTime? fechaRealizado = null,
                      DateTime? fechaEsperada = null, DateTime? fechaEntrega = null,
                      string estado = "", string comentarios = "",
                      int codCliente = -1)
        {
            Identificador = id;
            FechaRealizado = fechaRealizado;
            FechaEsperada = fechaEsperada;
            FechaEntrega = fechaEntrega;
            Estado = estado;
            Comentarios = comentarios;
            CodigoCliente = codCliente;
        }

        //
        // Getters
        //
        public int GetIdentificador() { return Identificador; }
        public DateTime? GetFechaRealizado() { return FechaRealizado; }
        public DateTime? GetFechaEsperada() { return FechaEsperada; }
        public DateTime? GetFechaEntrega() { return FechaEntrega; }
        public string GetEstado() { return Estado; }
        public string GetComentarios() { return Comentarios; }
        public int GetCodigoCliente() { return CodigoCliente; }
    }
}
