﻿namespace Jardineria.Modelos
{
    class Oficina
    {
        //
        // Atributos
        //
        private string Identificador;
        private string Ciudad;
        private string Pais;
        private string Region;
        private string CodigoPostal;
        private string Telefono;
        private string DireccionPrincipal;
        private string DireccionSecundaria;

        //
        // Constructor
        //
        public Oficina(string id = "", string ciudad = "",
                       string pais = "", string region = "",
                       string cp = "", string telf = "",
                       string direccion1 = "", string direccion2 = "")
        {
            Identificador = id;
            Ciudad = ciudad;
            Pais = pais;
            Region = region;
            CodigoPostal = cp;
            Telefono = telf;
            DireccionPrincipal = direccion1;
            DireccionSecundaria = direccion2;
        }

        //
        // Getters
        //
        public string GetIdentificador() { return Identificador; }
        public string GetCiudad() { return Ciudad; }
        public string GetPais() { return Pais; }
        public string GetRegion() { return Region; }
        public string GetCodigoPostal() { return CodigoPostal; }
        public string GetTelefono() { return Telefono; }
        public string GetDireccionPrincipal() { return DireccionPrincipal; }
        public string GetDireccionSecundaria() { return DireccionSecundaria; }
    }
}
