﻿namespace Jardineria.Modelos
{
    class Trabajador
    {
        //
        // Atributos
        //
        private int Identificador;
        private string Nombre;
        private string PrimerApellido;
        private string SegundoApellido;
        private string Extension;
        private string Email;
        private Oficina OficinaTrabajo;
        private string Puesto;

        //
        // Constructor
        //
        public Trabajador(int id = -1, string nombre = "",
                          string apellido1 = "", string apellido2 = "",
                          string extension = "", string email = "",
                          Oficina oficina = null, string puesto = "")
        {
            Identificador = id;
            Nombre = nombre;
            PrimerApellido = apellido1;
            SegundoApellido = apellido2;
            Extension = extension;
            Email = email;
            OficinaTrabajo = oficina;
            Puesto = puesto;
        }

        //
        // Getters
        //
        public int GetIdentificador() { return Identificador; }
        public string GetNombre() { return Nombre; }
        public string GetPrimerApellido() { return PrimerApellido; }
        public string GetSegundoApellido() { return SegundoApellido; }
        public string GetExtension() { return Extension; }
        public string GetEmail() { return Email; }
        public Oficina GetOficina() { return OficinaTrabajo; }
        public string GetPuesto() { return Puesto; }
    }
}
