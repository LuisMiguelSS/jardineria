﻿using Jardineria.Error.gui;
using Jardineria.Modelos.database;
using MySql.Data.MySqlClient;
using MySql.Data.Types;
using System;
using System.Collections.Generic;

namespace Jardineria.Modelos
{
    class BaseDatos
    {
        //
        // Atributos
        //
        private static BaseDatos instancia;
        private MySqlConnection conexion;

        //
        // Constructor
        //
        private BaseDatos()
        {
            conexion = Conexion.GetConexion();
        }

        //
        // Getters
        //
        public static BaseDatos GetInstanceOf()
        {
            if (instancia == null)
                instancia = new BaseDatos();

            return instancia;
        }
        public MySqlConnection GetConexion() { return conexion; }

        // SQL
        public static string GetString(MySqlDataReader reader, string columnName)
        {
            return reader[columnName] == DBNull.Value ? "" : (string)reader[columnName];
        }
        public static int GetInt(MySqlDataReader reader, string columnName)
        {
            return reader[columnName] == DBNull.Value ? -1 : (int)reader[columnName];
        }
        public DateTime? GetDate(MySqlDataReader reader, string columnName)
        {
            return reader[columnName] == DBNull.Value ? new DateTime?() : new MySqlDateTime((DateTime)reader[columnName]).Value;
        }

        //
        // Consultas fijas
        //
        public List<Producto> GetProductos()
        {
            List<Producto> listado = new List<Producto>();

            // Si la conexión está abierta, realizar consulta
            if (conexion.State == System.Data.ConnectionState.Open)
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand("SELECT * FROM producto ORDER BY codigo_producto", conexion))
                    {
                        using (MySqlDataReader r = command.ExecuteReader())
                        {
                            while (r.Read())
                                listado.Add(new Producto(
                                                GetString(r, "codigo_producto"),     // Código
                                                GetString(r, "nombre"),              // Nombre
                                                new Gama(GetString(r, "gama")),      // Gama
                                                GetString(r, "dimensiones"),         // Dimensiones
                                                GetString(r, "proveedor"),           // Proveedor
                                                GetString(r, "descripcion"),         // Descripción
                                                r.GetInt32("cantidad_en_stock"),     // Stock
                                                r.GetDouble(7),                      // PVP
                                                r.GetDouble(8)                       // Precio compra al proveedor
                                    ));
                        }
                    }
                }
                catch (MySqlException mse)
                {
                    new VentanaError("Error al trabajar con la base de datos", mse.Message).ShowDialog();
                }

            }
            else
            {
                new VentanaError("Error de Conexión", "No se ha establecido conexión con la base de datos previamente.").ShowDialog();
            }

            return listado;
        }
        public List<Producto> GetProductosLimite()
        {
            List<Producto> listado = new List<Producto>();

            // Si la conexión está abierta, realizar consulta
            if (conexion.State == System.Data.ConnectionState.Open)
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand("SELECT * FROM producto, (SELECT MAX(precio_venta) AS mayor, MIN(precio_venta) AS menor FROM producto) AS q WHERE precio_venta = q.mayor OR precio_venta = q.menor ORDER BY precio_venta DESC", conexion))
                    {
                        using (MySqlDataReader r = command.ExecuteReader())
                        {
                            while (r.Read())
                                listado.Add(new Producto(
                                                GetString(r, "codigo_producto"),     // Código
                                                GetString(r, "nombre"),              // Nombre
                                                new Gama(GetString(r, "gama")),      // Gama
                                                GetString(r, "dimensiones"),         // Dimensiones
                                                GetString(r, "proveedor"),           // Proveedor
                                                GetString(r, "descripcion"),         // Descripción
                                                r.GetInt32("cantidad_en_stock"),     // Stock
                                                r.GetDouble(7),                      // PVP
                                                r.GetDouble(8)                       // Precio compra al proveedor
                                    ));
                        }
                    }
                }
                catch (MySqlException mse)
                {
                    new VentanaError("Error al trabajar con la base de datos", mse.Message).ShowDialog();
                }

            }
            else
            {
                new VentanaError("Error de Conexión", "No se ha establecido conexión con la base de datos previamente.").ShowDialog();
            }

            return listado;
        }

        public List<Gama> GetGamas()
        {
            List<Gama> listado = new List<Gama>();

            // Si la conexión está abierta, realizar consulta
            if (conexion.State == System.Data.ConnectionState.Open)
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand("SELECT * FROM gama_producto ORDER BY gama", conexion))
                    {
                        using (MySqlDataReader r = command.ExecuteReader())
                        {
                            while (r.Read())
                                listado.Add(new Gama(
                                                GetString(r, "gama"),                // Nombre
                                                GetString(r, "descripcion_texto"),   // Descripción texto
                                                GetString(r, "descripcion_html"),    // Descripción HTML
                                                GetString(r, "imagen")               // Imagen
                                    ));
                        }
                    }
                }
                catch (MySqlException mse)
                {
                    new VentanaError("Error al trabajar con la base de datos", mse.Message).ShowDialog();
                }

            }
            else
            {
                new VentanaError("Error de Conexión", "No se ha establecido conexión con la base de datos previamente.").ShowDialog();
            }

            return listado;
        }
        public List<Pedido> GetEntregas(string condicion = "")
        {
            List<Pedido> listado = new List<Pedido>();

            // Si la conexión está abierta, realizar consulta
            if (conexion.State == System.Data.ConnectionState.Open)
            {

                try
                {
                    using (MySqlCommand command = new MySqlCommand("SELECT * FROM pedido WHERE fecha_entrega IS NOT NULL" +
                                                                (condicion.Trim() == "" ?
                                                                                    " " : " AND " + condicion + " ")
                                                                + " ORDER BY fecha_pedido DESC", conexion))
                    {
                        using (MySqlDataReader r = command.ExecuteReader())
                        {
                            while (r.Read())
                                listado.Add(new Pedido(
                                                r.GetInt32("codigo_pedido"),    // Identificador
                                                GetDate(r, "fecha_pedido"),     // Fecha Realización Pedido
                                                GetDate(r, "fecha_esperada"),   // Fecha Esperada Entrega
                                                GetDate(r, "fecha_entrega"),    // Fecha Real Entrega
                                                GetString(r, "estado"),         // Estado
                                                GetString(r, "comentarios"),    // Comentarios
                                                GetInt(r, "codigo_cliente")     // Código cliente
                                    ));
                        }
                    }
                }
                catch (MySqlException mse)
                {
                    new VentanaError("Error al trabajar con la base de datos", mse.Message).ShowDialog();
                }

            }
            else
            {
                new VentanaError("Error de Conexión", "No se ha establecido conexión con la base de datos previamente.").ShowDialog();
            }

            return listado;
        }
        public List<Jefe> GetJefes()
        {
            List<Jefe> listado = new List<Jefe>();

            // Si la conexión está abierta, realizar consulta
            if (conexion.State == System.Data.ConnectionState.Open)
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand("SELECT * FROM empleado WHERE codigo_empleado IN ( SELECT codigo_jefe FROM empleado ) ORDER BY nombre", conexion))
                    {
                        using (MySqlDataReader r = command.ExecuteReader())
                        {
                            while (r.Read())
                                listado.Add(new Jefe(
                                                    r.GetInt32("codigo_empleado"),               // Identificador
                                                    GetString(r, "nombre"),                      // Nombre
                                                    GetString(r, "apellido1"),                   // Apellido Principal
                                                    GetString(r, "apellido2"),                   // Apellido Secundario
                                                    GetString(r, "extension"),                   // Extensión
                                                    GetString(r, "email"),                       // Email
                                                    new Oficina(GetString(r, "codigo_oficina")), // Oficina
                                                    GetString(r, "puesto"),                      // Puesto
                                                    new List<Empleado>()                         // Lista Empleados a su cargo
                                ));
                        }

                    }
                }
                catch (MySqlException mse)
                {
                    new VentanaError("Error al trabajar con la base de datos", mse.Message).ShowDialog();
                }

            }
            else
            {
                new VentanaError("Error de Conexión", "No se ha establecido conexión con la base de datos previamente.").ShowDialog();
            }

            return listado;
        }
        public List<Empleado> GetEmpleados(string condicion = "")
        {
            List<Empleado> listado = new List<Empleado>();

            // Si la conexión está abierta, realizar consulta
            if (conexion.State == System.Data.ConnectionState.Open)
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand("SELECT * FROM empleado" +
                                                                (condicion.Trim() == "" ?
                                                                                    " " : " WHERE " + condicion + " ")
                                                                + "ORDER BY nombre", conexion))
                    {
                        using (MySqlDataReader r = command.ExecuteReader())
                        {
                            while (r.Read())
                                listado.Add(new Empleado(
                                                    r.GetInt32("codigo_empleado"),               // Identificador
                                                    GetString(r, "nombre"),                      // Nombre
                                                    GetString(r, "apellido1"),                   // Apellido Principal
                                                    GetString(r, "apellido2"),                   // Apellido Secundario
                                                    GetString(r, "extension"),                   // Extensión
                                                    GetString(r, "email"),                       // Email
                                                    new Oficina(GetString(r, "codigo_oficina")), // Oficina
                                                    GetString(r, "puesto"),                      // Puesto
                                                    new Jefe(GetInt(r, "codigo_jefe"))           // Jefe
                                ));
                        }
                    }
                }
                catch (MySqlException mse)
                {
                    new VentanaError("Error al trabajar con la base de datos", mse.Message).ShowDialog();
                }


            }
            else
            {
                new VentanaError("Error de Conexión", "No se ha establecido conexión con la base de datos previamente.").ShowDialog();
            }

            return listado;
        }
        public List<Cliente> GetClientes(string condicion = "")
        {
            List<Cliente> listado = new List<Cliente>();

            // Si la conexión está abierta, realizar consulta
            if (conexion.State == System.Data.ConnectionState.Open)
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand("SELECT * FROM cliente" +
                                                                (condicion.Trim() == "" ?
                                                                                    " " : " WHERE " + condicion + " ")
                                                                + "ORDER BY codigo_cliente", conexion))
                    {
                        using (MySqlDataReader r = command.ExecuteReader())
                        {
                            while (r.Read())
                                listado.Add(new Cliente(
                                                    r.GetInt32("codigo_cliente"),                               // Identificador
                                                    GetString(r, "nombre_cliente"),                             // Nombre
                                                    GetString(r, "nombre_contacto"),                            // Nombre Contacto
                                                    GetString(r, "apellido_contacto"),                          // Apellido Contacto
                                                    GetString(r, "telefono"),                                   // Telefono
                                                    GetString(r, "fax"),                                        // Fax
                                                    GetString(r, "linea_direccion1"),                           // Dirección Principal
                                                    GetString(r, "linea_direccion2"),                           // Dirección Alternativa
                                                    GetString(r, "ciudad"),                                     // Ciudad
                                                    GetString(r, "region"),                                     // Región
                                                    GetString(r, "pais"),                                       // País
                                                    GetString(r, "codigo_postal"),                              // Código Postal
                                                    new Trabajador(r.GetInt32("codigo_empleado_rep_ventas")),   // Representante
                                                    r.GetDouble("limite_credito")                               // Límite de Crédito
                                ));
                        }
                    }
                }
                catch (MySqlException mse)
                {
                    new VentanaError("Error al trabajar con la base de datos", mse.Message).ShowDialog();
                }


            }
            else
            {
                new VentanaError("Error de Conexión", "No se ha establecido conexión con la base de datos previamente.").ShowDialog();
            }

            return listado;
        }
        public List<Cliente> GetDeudores()
        {
            return GetClientes(
                    "codigo_cliente IN (SELECT codigo_cliente FROM pedido) " +
                    "AND codigo_cliente NOT IN(SELECT codigo_cliente FROM pago)"
                );
        }

        public Boolean AddProducto(Producto producto)
        {
            // Si la conexión está abierta, realizar consulta
            if (conexion.State == System.Data.ConnectionState.Open)
            {
                try
                {
                    using (MySqlCommand command = new MySqlCommand("INSERT INTO producto VALUES(@1, @2, @3, @4, @5, @6, @7, @8, @9)", conexion))
                    {
                        command.Parameters.AddWithValue("@1", producto.GetCodigo());
                        command.Parameters.AddWithValue("@2", producto.GetNombre());
                        command.Parameters.AddWithValue("@3", producto.GetGama().GetNombre());
                        command.Parameters.AddWithValue("@4", producto.GetDimensiones());
                        command.Parameters.AddWithValue("@5", producto.GetProveedor());
                        command.Parameters.AddWithValue("@6", producto.GetDescripcion());
                        command.Parameters.AddWithValue("@7", producto.GetStock());
                        command.Parameters.AddWithValue("@8", producto.GetPVP());
                        command.Parameters.AddWithValue("@9", producto.GetPrecioProveedor());

                        return command.ExecuteNonQuery() > 0;
                    }
                }
                catch (MySqlException mse)
                {
                    new VentanaError("Error al trabajar con la base de datos", mse.Message).ShowDialog();
                    return false;
                }


            }

            return false;
        }

        public void RemoveGama(string name)
        {
            // Si la conexión está abierta, realizar consulta
            if (conexion.State == System.Data.ConnectionState.Open)
            {
                try
                {

                    using (MySqlCommand command = new MySqlCommand("DELETE FROM producto WHERE gama = @1", conexion))
                    {
                        command.Parameters.AddWithValue("@1", name);

                        // Borrar referencia
                        command.ExecuteNonQuery();

                        // Borrar gama
                        command.CommandText = "DELETE FROM gama_producto WHERE gama = @1";
                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@1", name);

                        command.ExecuteNonQuery();
                    }
                }
                catch (MySqlException mse)
                {
                    new VentanaError("Error al trabajar con la base de datos", mse.Message).ShowDialog();
                }

            }
        }

        //
        // Otros métodos
        //
        /// Conectar - establece la conexión con la base de datos
        /// <exception cref="System.InvalidOperationException">Ocurre al intentar llamar a este método siendo la instancia de esta clase errónea.</exception>
        /// <exception cref="MySqlException">Puede ocurrir cuando se intenta conectar a la base de datos con una cadena de conexión errónea de la referencia de MySql o cuando la base de datos no se encuentra operativa.</exception>
        public void Conectar()
        {
            if (instancia != null)
                conexion.Open();
        }
    }
}
