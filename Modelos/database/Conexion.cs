﻿using MySql.Data.MySqlClient;

namespace Jardineria.Modelos.database
{
    class Conexion
    {
        //
        // Atributos
        //
        public static readonly string SERVER = "127.0.0.1";
        public static readonly string DATABASE = "jardineria";
        public static readonly string UID = "root";
        public static readonly string PASSWORD = "";

        //
        // Otros métodos
        //
        public static MySqlConnection GetConexion()
        {
            return new MySqlConnection(
                "server=" + SERVER + ";" +
                "database=" + DATABASE + ";" +
                "Uid=" + UID + ";" +
                "pwd=" + PASSWORD + ";"
                );
        }
    }
}
