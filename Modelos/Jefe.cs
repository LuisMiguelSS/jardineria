﻿using System.Collections.Generic;

namespace Jardineria.Modelos
{
    class Jefe : Trabajador
    {
        //
        // Atributos
        //
        private List<Empleado> Personal;

        //
        // Constructores
        //
        public Jefe() : base() { }
        public Jefe(int id = -1, string nombre = "",
                    string apellido1 = "", string apellido2 = "",
                    string extension = "", string email = "",
                    Oficina oficina = null, string puesto = "",
                    List<Empleado> listaPersonal = null) : base(id, nombre,
                                                                apellido1, apellido2,
                                                                extension, email,
                                                                oficina, puesto)
        {
            Personal = listaPersonal;
        }

        //
        // Getters
        //
        public List<Empleado> GetPersonal() { return Personal; }

        //
        // Setters
        //
        public void SetPersonal(List<Empleado> listaEmpleados)
        {
            Personal = listaEmpleados;
        }
    }
}
