﻿namespace Jardineria.Modelos
{
    class Empleado : Trabajador
    {
        //
        // Atributos
        //
        private Jefe Superior;

        //
        // Constructores
        //
        public Empleado() : base() { }
        public Empleado(int id = -1, string nombre = "",
                    string apellido1 = "", string apellido2 = "",
                    string extension = "", string email = "",
                    Oficina oficina = null, string puesto = "",
                    Jefe superior = null) : base(id, nombre,
                                                 apellido1, apellido2,
                                                 extension, email,
                                                 oficina, puesto)
        {
            Superior = superior;
        }

        //
        // Getters
        //
        public Jefe GetJefe() { return Superior; }

        //
        // Setters
        //
        public void SetPersonal(Jefe superior)
        {
            Superior = superior;
        }
    }
}
