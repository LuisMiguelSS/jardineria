﻿namespace Jardineria.Modelos
{
    public class Producto
    {
        //
        // Atributos
        //
        private string CodigoProducto;
        private string Nombre;
        private Gama GamaProducto;
        private string Dimensiones;
        private string Proveedor;
        private string Descripcion;
        private int Stock;
        private double PVP;
        private double PrecioProveedor;

        //
        // Constructor
        //
        public Producto(string codigo = "Desc", string nombre = "Desconocido",
                        Gama gama = null, string dimensiones = "Desconocidas",
                        string proveedor = "Desconocido", string descripcion = "",
                        int stock = 0, double pvp = 0.0,
                        double precioProveedor = 0.0)
        {
            CodigoProducto = codigo;
            Nombre = nombre;
            GamaProducto = gama;
            Dimensiones = dimensiones;
            Proveedor = proveedor;
            Descripcion = descripcion;
            Stock = stock;
            PVP = pvp;
            PrecioProveedor = precioProveedor;
        }

        //
        // Getters
        //
        public string GetCodigo() { return CodigoProducto; }
        public string GetNombre() { return Nombre; }
        public Gama GetGama() { return GamaProducto; }
        public string GetDimensiones() { return Dimensiones; }
        public string GetProveedor() { return Proveedor; }
        public string GetDescripcion() { return Descripcion; }
        public int GetStock() { return Stock; }
        public double GetPVP() { return PVP; }
        public double GetPrecioProveedor() { return PrecioProveedor; }

        //
        // Otros métodos
        //
        public override string ToString()
        {
            return "Producto { código = " + CodigoProducto + ", " +
                              "nombre = " + Nombre + ", " +
                              "Gama = " + GamaProducto.ToString() + ", " +
                              "dimensiones = " + Dimensiones + ", " +
                              "proveedor = " + Proveedor + ", " +
                              "descripcion = " + Descripcion + ", " +
                              "stock = " + Stock + ", " +
                              "PVP = " + PVP + "€, " +
                              "precio proveedor = " + PrecioProveedor + "}";
        }
    }
}
