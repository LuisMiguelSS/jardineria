﻿namespace Jardineria.Modelos
{
    class Cliente
    {
        //
        // Atributos
        //
        private int Identificador;
        private string Nombre;
        private string NombreContacto;
        private string ApellidoContacto;
        private string Telefono;
        private string Fax;
        private string DireccionPrincipal;
        private string DireccionAlternativa;
        private string Ciudad;
        private string Region;
        private string Pais;
        private string CodigoPostal;
        private Trabajador Representante;
        private double LimiteCredito;

        //
        // Constructor
        //
        public Cliente(int id = -1, string nombre = "",
                        string nombreContacto = "", string apellidoContacto = "",
                        string telefono = "", string fax = "",
                        string dirPrincipal = "", string dirAlternativa = "",
                        string ciudad = "", string region = "",
                        string pais = "", string cp = "",
                        Trabajador representante = null, double limiteCredito = 0.0)
        {
            Identificador = id;
            Nombre = nombre;
            NombreContacto = nombreContacto;
            ApellidoContacto = apellidoContacto;
            Telefono = telefono;
            Fax = fax;
            DireccionPrincipal = dirPrincipal;
            DireccionAlternativa = dirAlternativa;
            Ciudad = ciudad;
            Region = region;
            Pais = pais;
            CodigoPostal = cp;
            Representante = representante;
            LimiteCredito = limiteCredito;
        }

        //
        // Getters
        //
        public int GetIdentificador() { return Identificador; }
        public string GetNombre() { return Nombre; }
        public string GetNombreContacto() { return NombreContacto; }
        public string GetApellidoContacto() { return ApellidoContacto; }
        public string GetTelefono() { return Telefono; }
        public string GetFax() { return Fax; }
        public string GetDireccionPrincipal() { return DireccionPrincipal; }
        public string GetDireccionAlternativa() { return DireccionAlternativa; }
        public string GetCiudad() { return Ciudad; }
        public string GetRegion() { return Region; }
        public string GetPais() { return Pais; }
        public string GetCodigoPostal() { return CodigoPostal; }
        public Trabajador GetRepresentante() { return Representante; }
        public double GetLimiteCredito() { return LimiteCredito; }
    }
}
