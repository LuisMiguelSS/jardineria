﻿using Jardineria.Error.gui;
using Jardineria.Modelos;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Jardineria
{
    public partial class VentanaPrincipal : Form
    {
        //
        // Atributos
        //
        private int NumeroResultados = 0;
        private BaseDatos db;
        private string ComboBoxFechasSeleccion = "";

        //
        // Constructor
        //
        public VentanaPrincipal()
        {
            InitializeComponent();

            db = BaseDatos.GetInstanceOf();
            try
            {
                db.Conectar();

                // Actualizar contador
                CargarProductos();

                // Recargar el número de resultados
                LabelNumeroRegistros.Text = "Nº de resultados: " + --NumeroResultados;

            }
            catch (Exception e)
            {
                if (e is InvalidOperationException)
                    new VentanaError("Error del programa", "No se ha podido preparar el programa correctamente, por favor, contacte al desarrollador.").ShowDialog();
                else if (e is MySqlException)
                    new VentanaError("Error de la base de datos", e.Message).ShowDialog();
                else
                    new VentanaError("Error", e.Message).ShowDialog();

            }

        }

        //
        // Otros métodos
        //
        public void CargarProductos()
        {
            NumeroResultados = 1;
            ListaProductos.Items.Clear();

            // Cargar productos en lista
            foreach (Producto p in db.GetProductos())
            {
                string[] fila = { NumeroResultados++.ToString(), p.GetCodigo(), p.GetNombre(), p.GetGama().GetNombre(), p.GetStock().ToString(), p.GetPVP() + "€" };
                ListaProductos.Items.Add(new ListViewItem(fila));
            }

            // Cambiar estilos de la lista
            foreach (ColumnHeader c in ListaProductos.Columns)
                c.Width = -2;

            // Cargar gamas en comboBox
            ComboBoxGama.Items.Clear();
            foreach (Gama g in db.GetGamas())
                ComboBoxGama.Items.Add(g.GetNombre());
        }

        public void CargarGamas()
        {
            NumeroResultados = 1;
            ListaGamas.Items.Clear();

            // Cargar productos en lista
            foreach (Gama g in db.GetGamas())
            {
                string[] fila = { NumeroResultados++.ToString(), g.GetNombre(), g.GetDescripcion() };
                ListaGamas.Items.Add(new ListViewItem(fila));
            }

            // Cambiar estilos de la lista
            foreach (ColumnHeader c in ListaGamas.Columns)
                c.Width = -2;
        }

        public void CargarEntregas(string condicion = "")
        {
            NumeroResultados = 1;
            ListaEntregas.Items.Clear();

            // Cargar productos en lista
            foreach (Pedido p in db.GetEntregas(condicion))
            {
                string[] fila = { NumeroResultados++.ToString(), p.GetIdentificador().ToString(), p.GetFechaRealizado().Value.ToShortDateString(),
                                    p.GetFechaEsperada().Value.ToShortDateString(), p.GetFechaEntrega().Value.ToShortDateString(), p.GetComentarios().ToString(), p.GetCodigoCliente().ToString() };
                ListaEntregas.Items.Add(new ListViewItem(fila));
            }

            // Cambiar estilos de la lista
            foreach (ColumnHeader c in ListaEntregas.Columns)
                c.Width = -2;
        }

        // Necesita ser privado para evitar "incoherencias de nivel de acceso"
        private void CargarEmpleados(List<Empleado> empleadosACargar = null)
        {
            NumeroResultados = 1;
            ListaEmpleados.Items.Clear();

            // Cargar productos en lista
            foreach (Empleado e in empleadosACargar == null ? db.GetEmpleados() : empleadosACargar)
            {
                string[] fila = { NumeroResultados++.ToString(), e.GetNombre(), e.GetPrimerApellido() + " " + e.GetSegundoApellido(),
                                    e.GetEmail() };
                ListaEmpleados.Items.Add(new ListViewItem(fila));
            }

            // Cambiar estilos de la lista
            foreach (ColumnHeader c in ListaEmpleados.Columns)
                c.Width = -2;
        }

        public void CargarDeudores()
        {
            NumeroResultados = 1;
            ListaDeudores.Items.Clear();

            // Cargar productos en lista
            foreach (Cliente c in db.GetDeudores())
            {
                string[] fila = { NumeroResultados++.ToString(), c.GetIdentificador().ToString(), c.GetNombre(), c.GetNombreContacto() + " " + c.GetApellidoContacto(),
                                    c.GetTelefono(), c.GetFax(), c.GetDireccionPrincipal(), c.GetDireccionAlternativa(),
                                    c.GetCiudad(), c.GetRegion(), c.GetPais(), c.GetCodigoPostal(), c.GetRepresentante().GetIdentificador().ToString(),
                                    c.GetLimiteCredito().ToString()
                };
                ListaDeudores.Items.Add(new ListViewItem(fila));
            }

            // Cambiar estilos de la lista
            foreach (ColumnHeader c in ListaDeudores.Columns)
                c.Width = -2;
        }

        public Boolean CamposProductoValidos()
        {
            // ID
            if (string.IsNullOrEmpty(TextBoxIdentificador.Text.Trim()) || TextBoxIdentificador.Text.Trim().Length > 15)
            {
                MessageBox.Show("El campo \"Identificador\" no puede estar vacío ni exceder un máximo de 15 caracteres.", "Error al Añadir",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            // Nombre
            else if (string.IsNullOrEmpty(TextBoxNombre.Text.Trim()) || TextBoxNombre.Text.Trim().Length > 70)
            {
                MessageBox.Show("El campo \"Nombre\" no puede estar vacío ni exceder un máximo de 70 caracteres.", "Error al Añadir",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            // Gama
            else if (string.IsNullOrEmpty(ComboBoxGama.Text.Trim()) || ComboBoxGama.Text.Trim().Length > 50)
            {
                MessageBox.Show("El campo \"Gama\" no puede estar vacío ni exceder un máximo de 50 caracteres.", "Error al Añadir",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            // Dimensiones
            else if (string.IsNullOrEmpty(TextBoxDimensiones.Text.Trim()) || TextBoxDimensiones.Text.Trim().Length > 25)
            {
                MessageBox.Show("El campo \"Dimensiones\" no puede estar vacío ni exceder un máximo de 25 caracteres.", "Error al Añadir",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            // Proveedor
            else if (string.IsNullOrEmpty(TextBoxProveedor.Text.Trim()) || TextBoxProveedor.Text.Trim().Length > 50)
            {
                MessageBox.Show("El campo \"Proveedor\" no puede estar vacío ni exceder un máximo de 50 caracteres.", "Error al Añadir",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            // Descripción
            else if (string.IsNullOrEmpty(TextBoxDescripcion.Text.Trim()) || TextBoxDescripcion.Text.Trim().Length > 65535)
            {
                MessageBox.Show("El campo \"Descripción\" no puede estar vacío ni exceder un máximo de 65535 caracteres.", "Error al Añadir",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            return true;
        }
        public int GetMesPorNombre(string nombre)
        {
            switch (nombre.Trim().ToLower())
            {
                case "enero":
                    return 1;
                case "febrero":
                    return 2;
                case "marzo":
                    return 3;
                case "abril":
                    return 4;
                case "mayo":
                    return 5;
                case "junio":
                    return 6;
                case "julio":
                    return 7;
                case "agosto":
                    return 8;
                case "septiembre":
                    return 9;
                case "octubre":
                    return 10;
                case "noviembre":
                    return 11;
                case "diciembre":
                    return 12;
                default:
                    return -1;
            }
        }

        //
        // Eventos
        //
        private void PanelTabulado_Selecting(object sender = null, TabControlCancelEventArgs e = null)
        {
            if (e != null)
            {
                // Cargar resultados
                switch (e.TabPage.Text)
                {
                    case "Productos":
                        CargarProductos();
                        break;
                    case "Gamas":
                        CargarGamas();
                        break;
                    case "Entregas":
                        CargarEntregas();
                        break;
                    case "Empleados":
                        foreach (Jefe j in db.GetJefes())
                            ListaJefes.Items.Add(j.GetNombre() + " " + j.GetPrimerApellido() +
                                                 " (" + j.GetIdentificador() + ")");
                        CargarEmpleados();
                        break;
                    case "Deudores":
                        CargarDeudores();
                        break;
                    default:
                        break;
                }
            }

            // Recargar el número de resultados
            LabelNumeroRegistros.Text = "Nº de resultados: " + --NumeroResultados;
        }

        private void ListaJefes_DropDownClosed(object sender, EventArgs e)
        {
            if (ListaJefes.Text != "")
                CargarEmpleados(db.GetEmpleados("codigo_jefe = " + ListaJefes.Text.Split('(', ')')[1]));
        }

        private void ButtonLimpiar_Click(object sender, EventArgs e)
        {
            TextBoxIdentificador.Text = "";
            TextBoxNombre.Text = "";
            ComboBoxGama.Text = "";
            TextBoxDimensiones.Text = "";
            TextBoxProveedor.Text = "";
            TextBoxDescripcion.Text = "";
            NumericStock.Value = 0;
            NumericPVP.Value = 0;
            NumericPrecioProveedor.Value = 0;
        }

        private void ButtonAnadir_Click(object sender, EventArgs e)
        {
            if (CamposProductoValidos())
            {
                Producto p = new Producto(
                                           TextBoxIdentificador.Text.Trim(),
                                           TextBoxNombre.Text.Trim(),
                                           new Gama(ComboBoxGama.Text.Trim()),
                                           TextBoxDimensiones.Text.Trim(),
                                           TextBoxProveedor.Text.Trim(),
                                           TextBoxDescripcion.Text.Trim(),
                                           Decimal.ToInt32(NumericStock.Value),
                                           Decimal.ToDouble(NumericPVP.Value),
                                           Decimal.ToDouble(NumericPrecioProveedor.Value));

                if (db.AddProducto(p))
                {
                    MessageBox.Show("El producto ha sido añadido con éxito.", "Producto Añadido",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CargarProductos();
                }
                else
                    MessageBox.Show("El producto no se ha podido añadir.", "Producto No Añadido",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ButtonEliminarGama_Click(object sender, EventArgs e)
        {
            if (ListaGamas.SelectedItems.Count > 0)
            {
                string NombreGama = ListaGamas.SelectedItems[0].SubItems[1].Text;

                if (MessageBox.Show("¿Está seguro de que desea eliminar la gama \"" + NombreGama + "\"?\r\n*Atención: esto borrará todos los registros asociados a ella.",
                                   "Eliminar Gama", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    db.RemoveGama(NombreGama);
                    CargarGamas();
                    LabelNumeroRegistros.Text = "Nº de resultados: " + --NumeroResultados;
                }
            }
        }

        private void ComboBoxFechas_DropDownClosed(object sender, EventArgs e)
        {
            if (ComboBoxFechas.Text != ComboBoxFechasSeleccion)
            {
                if (ComboBoxFechas.Text == "")
                    CargarEntregas();
                else
                    CargarEntregas("MONTH(fecha_entrega) = " + GetMesPorNombre(ComboBoxFechas.Text));

                ComboBoxFechasSeleccion = ComboBoxFechas.Text;
            }
        }

        private void MenuInicioDesconectar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void VentanaPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("¿Está seguro de que desea salir?", "Confirmación de Salida",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                e.Cancel = true;
        }

        private void másCaroYMásBaratoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<Producto> productosLimite = db.GetProductosLimite();

            if (productosLimite.Count > 1)
                new VentanaPreciosLimite(productosLimite[0], productosLimite[productosLimite.Count - 1]).Show();
            else
                MessageBox.Show("No hay suficientes productos para realizar los cálculos o todos tienen el mismo precio.", "Cálculo Inválido",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
    }
}
